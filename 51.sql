--51.แสดงจำนวนของสมัยการปกครองและจำนวนของรองประธานาธิบดีของสมัยการปกคอรงที่มีรองประธานาธิบดีมากกว่า 1 คน
select pres_name, count(pres_name), count(vice_pres_name)
from admin_pr_vp apv 
group by pres_name 
having count(vice_pres_name) > 1
